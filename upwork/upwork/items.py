# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field


class UpworkItem(Item):
    name = Field()
    #name2 = Field()
    site = Field()
    phone = Field()
    fax = Field()
    plz = Field()
    city = Field()
    street = Field()
