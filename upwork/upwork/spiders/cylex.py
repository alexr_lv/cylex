# -*- coding: utf-8 -*-
import scrapy
import urlparse
import re
from upwork.items import UpworkItem

class CylexSpider(scrapy.Spider):
    name = "cylex"
    allowed_domains = ["web2.cylex.de"]
    #start_urls = (
    #    'http://web2.cylex.de/s?q=landwirtschaft&c=&z=&p=1&dst=&cUrl=&geo=51.4590647,%2013.8656245&r=50',
    #)

    def start_requests(self):
        with open('test_urls.txt', 'r') as urls:
            for url in urls:
                yield scrapy.Request(url.strip(), self.parse)


    def parse(self, response):
        for result in response.xpath('//div[@class="lm-result-companyData ab-test"]'):
            path = result.xpath('div/div/h2[@itemprop="name"]/a/@href').extract()[0]
            url = urlparse.urlparse(response.url)
            url = url.scheme+"://"+url.netloc+path
            yield scrapy.Request(url, callback=self.parse_result)

        if response.xpath('//ul[@class="pagination"]'):
            current = re.search('(?<=p=)(\d)', response.url).group()
            pages = response.xpath('//ul[@class="pagination"]/li/a/text()').extract()

            if int(current) < int(pages[-1]):
                next = re.sub('p=\d','p='+str(int(current)+1), response.url)
                yield scrapy.Request(next, callback=self.parse)


    def parse_result(selfself, response):
        item = UpworkItem()
        dtls = response.xpath('//div[@class="prmry-cntct-dtls"]')
        item['name'] = dtls.xpath('div/span[@id="cntct-name"]/text()').extract()
        item['street'] = dtls.xpath('div/div/span[@itemprop="streetAddress"]/text()').extract()
        item['plz'] =  dtls.xpath('div/div/span[@itemprop="postalCode"]/text()').extract()
        item['city'] = dtls.xpath('div/div/span[@itemprop="addressLocality"]/text()').extract()

        phone = dtls.xpath('//div[@id="secondary-details"]/div/div/div[@itemprop="telephone"]/text()').extract()
        if phone:
            item['phone'] = phone
        fax =  dtls.xpath('//div[@id="secondary-details"]/div/div/div[@itemprop="faxNumber"]/text()').extract()
        if fax:
            item['fax'] = fax
        site = dtls.xpath('//span[@itemprop="url"]/@content').extract()
        if site:
            item['site'] = site



        yield item
